package main

import (
	"fmt"
	"sync"
)

// Организовать код так, чтобы одновременно выполнялось не более 5 горутин.

func startThread(i int, quotaChan chan struct{}, wg *sync.WaitGroup) {
	defer wg.Done()
	quotaChan <- struct{}{} // Занимаем слот в канале.
							// Если места не будет, то горутина будет ждать и не начнет работу, пока не освободиться место
	fmt.Println(i)
	<-quotaChan 			// Освобождаем слот
}
func threads() {
	var wg sync.WaitGroup
	quotaChan := make(chan struct{}, 5) // quotaChan представляет собой подсчитывающий семафор,
										// используемый для ограничения кол-ва выполняемых горутин величиной 5
	for i := 0; i < 100; i++ {
		wg.Add(1)
		go startThread(i, quotaChan, &wg)
	}
	wg.Wait()        // ожидаем завершения всех горутин
	close(quotaChan)
}
func main() {
	threads()
}

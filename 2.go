package main

import (
	"fmt"
	"sync"
)
//Необходимо вывести по 3 цифры в строке с использовать WaitGroup, так же надо избавиться от Sleep.

func printVal(j int, wg *sync.WaitGroup) {
	fmt.Print(j)
	wg.Done() // уменьшаем количество выполненых горутин
	}

func group() {
	for i := 0; i < 5; i++ {
		var wg sync.WaitGroup
		wg.Add(3) // группа на 3 горутины
		for j := 1; j <= 3; j++ {
			go printVal(j, &wg)
		}
		wg.Wait()        // ожидаем завершения горутин
		fmt.Println()
	}
}
func main() {
	group()
}
